<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Post;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */

    public function update(User $user, Post $post)
    {
        return $user->id == $post->users_id;
    }

    public function delete(User $user, Post $post)
    {
        return $user->id == $post->users_id;
    }
}
