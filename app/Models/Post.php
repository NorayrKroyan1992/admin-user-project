<?php

namespace App\Models;

use App\Http\Controllers\User\PostController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'img_name',
        'users_id'
    ];

    public function getUser()
    {
        return $this->belongsTo(\App\Models\User::class,'users_id', 'id');
    }
}
