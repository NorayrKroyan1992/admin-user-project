<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ROLE_ADMIN_ID = 1;
    const ROLE_USER_ID = 2;

    use HasFactory;
}
