<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditValidationRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Requests\PostValidationRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class PostController extends Controller
{
    public function index()
    {
        $posts = Post::orderby('id', 'Desc')->with('getUser')->get();

        return view('user.index', compact('posts'));
    }

    public function create(PostValidationRequest $request)
    {
        $input = $request->all();
        $validated = $request->validated();

        if ($request->hasfile('image')) {
            $image = $request->file('image');
            $destinationPath = 'public/img';
            $imageName = time() . $image->getClientOriginalName();
            $path = $image->storeAs($destinationPath, $imageName);
            $input['img_name'] = $imageName;
            $input['users_id'] = auth()->id();
        }

        Post::create($input);
        return back()->with('success','Post created successfully!!!');
    }

    public function edit(Request $request, $id)
    {
        $post = Post::find($id);
        return view('user.edit', compact('post'));
    }

    public function update (PostValidationRequest $request, $id)
    {
        $input = $request->all();
        $post = Post::find($id);


        if ($request->hasfile('image')) {
            $image = $request->file('image');
            $destinationPath = 'public/img/';
            $imageName = time() . $image->getClientOriginalName();
            unlink(storage_path('app/public/img/' . $post['img_name']));
            $image->storeAs($destinationPath, $imageName);
            $input['img_name'] = $imageName;
            $input['users_id'] = auth()->id();
        }

        $post->update($input);
        return redirect()->route('post.index')->with('success','Post waas updated successfully!');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $path = storage_path('/app/public/img/'. $post['img_name']);
        unlink($path);
        $post->delete();
        return back()->with('success','Post deleted successfully!!!');
    }
}
