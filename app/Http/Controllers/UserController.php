<?php

namespace App\Http\Controllers;


use App\Mail\UserNotification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use function Illuminate\Support\Facades\Date;
use App\Http\Requests\UserCreateValidationRequest;
use App\Http\Requests\UserPasswordValidationRequest;
use App\Http\Requests\UserEditValidationRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function passwordVerify() {
        return view('user.passwordVerify');
    }

    public  function passSave(UserPasswordValidationRequest $request)
    {
        $input = $request->all();
        $token = $input['api_token'];
        $user = User::where('token', $token)->first();
        $user->password = Hash::make($input['password']);
        $user->email_verified_at = now();
        $user->token = null;
        $user->save();
        auth()->login($user);
        return redirect('home');
    }

}
