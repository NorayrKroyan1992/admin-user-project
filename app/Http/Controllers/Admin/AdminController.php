<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateValidationRequest;
use App\Http\Requests\UserEditValidationRequest;
use App\Http\Requests\UserPasswordValidationRequest;
use App\Mail\UserNotification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::orderby('id', 'Desc')->where('role_id', 2)->get();
        return view('admin.home', compact('users'));
    }

    public function create(UserCreateValidationRequest $request)
    {
        $validated = $request->validated();
        $validated['token'] = bin2hex(random_bytes(10));
        $user = User::create($validated);
        Mail::to($validated['email'])->send(new UserNotification($user));
        return back()->with('success','User created successfully, waiting for verification by User');
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        return view('admin.edit', compact('user'));
    }

    public function update (UserEditValidationRequest $request, $id)
    {
        $user = User::find($id);
        $input = $request->all();


        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));
        }

        $user->update($input);
        return redirect()->route('admin.home')->with('success','User updated successfully!');
    }

    public function destroy ($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('admin.home')->with('success','User deleted successfully!');
    }
}
