@extends('layouts.app')

@section('content')



    {!! Form::model($user, ['method' => 'POST','route' => ['update', $user->id]]) !!}

    <div class="container">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> Something went wrong.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="col-sm-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <strong>Email:</strong><br>
                    <span>{{$user->email}}</span>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <strong>Password:</strong><br>
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
            </div>

                <div class="d-grid gap-2 pt-4">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <a class="btn btn-success" href="{{ route('admin.home') }}">Go Back Home</a>
                </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection

