@extends('layouts.app')

@section('content')



    {!! Form::model($post, ['method' => 'POST','route' => ['post.update', $post->id], 'files' => true]) !!}

    <div class="container">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> Something went wrong.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="col-sm-12">
                <div class="form-group">
                    <strong>Title:</strong>
                    {!! Form::text('title', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                </div>
            </div>

            <div class="form-group">
                <strong>Upload profile Images:</strong>
                <br>
                {!! Form::file('image', array('class' => 'form-control')) !!}
            </div>

            <div class="d-grid gap-2 pt-4">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a class="btn btn-success" href="{{ route('post.index') }}">Go Back Home</a>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection

