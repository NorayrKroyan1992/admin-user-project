@extends('layouts.app')

@section('content')

    @if($errors->any())
        <script>
            var modalShow = true;
        </script>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <table class="table table-bordered" id="user-table">
                    <tr>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($posts as  $key => $post)
                        <tr class="user-table-row">
                            <td>{{ $post->getUser->name }}</td>
                            <td>{{ $post->title }}</td>
                            <td><img src="{{ asset('/storage/img/'. $post['img_name']) }}" style="height:100px"></td>
                            <td>
                                <div class="d-flex">
                                    @can('update', $post)
                                    <a class="btn btn-primary" href="{{route('post.edit', ['id' => $post->id])}}">Edit</a>
                                    @endcan

                                    @can('delete', $post)
                                        <form action="{{route('post.delete', ['id' => $post->id])}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Create New Post
                </button>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Please make your POST</h5>
                    <button type="button" id="modal-close" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="modal-body">
                    {!! Form::model($posts, ['method' => 'POST','route' => ['post.create'], 'files' => true]) !!}
                    <div class="row">
                        <div class="form-group">
                            <strong>Title:</strong>
                            {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            <strong>Upload profile Images:</strong>
                            <br>
                            {!! Form::file('image', array('class' => 'form-control', 'files'=>true)) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add Post</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection
