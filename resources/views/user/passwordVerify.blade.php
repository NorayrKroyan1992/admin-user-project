@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h1>Please enter your password</h1>
<form action="{{route('password.save')}}" method="post" >
    @csrf
    <input type="hidden" name="api_token" value="{{request()->get('token')}}">
    <input type="password" name="password" placeholder="Enter Password" class="form-control">
    <button type="submit" class="btn btn-primary">Submit</button>
</form>


