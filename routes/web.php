<?php

use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\User\HomeController;
use Illuminate\Auth\Middleware\EnsureEmailIsVerified;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/user/verify', [UserController::class, 'passwordVerify'])->name('password.verify');
Route::post('/user/verify', [UserController::class, 'passSave'])->name('password.save');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/index', [App\Http\Controllers\User\PostController::class, 'index'])->name('post.index');
Route::post('/home/index/create', [App\Http\Controllers\User\PostController::class, 'create'])->name('post.create');
Route::get('/home/index/edit/{id}', [App\Http\Controllers\User\PostController::class, 'edit'])->name('post.edit');
Route::post('/home/index/edit/{id}', [App\Http\Controllers\User\PostController::class, 'update'])->name('post.update');
Route::delete('/home/index/delete/{id}', [App\Http\Controllers\User\PostController::class, 'destroy'])->name('post.delete');

Route::post('email/verify', [VerificationController::class, 'show'])->name('verification.notice');

Route::group(['middleware' => ['admin']], function() {
    Route::get('/admin/home', [App\Http\Controllers\Admin\AdminController::class, 'index'])->name('admin.home');
    Route::post('/admin/home/create', [App\Http\Controllers\Admin\AdminController::class, 'create'])->name('admin.create');
    Route::get('/admin/home/edit/{id}', [App\Http\Controllers\Admin\AdminController::class, 'edit'])->name('admin.edit');
    Route::post('/admin/home/update/{id}', [App\Http\Controllers\Admin\AdminController::class, 'update'])->name('update');
    Route::delete('/admin/home/delete/{id}', [App\Http\Controllers\Admin\AdminController::class, 'destroy'])->name('user.delete');
});


